<?php
namespace Dracoder\Extensions;

use DateTime;

class StringExtension
{
    /** @var string $string */
    private $string;

    /**
     * StringExtension constructor.
     *
     * @param string $string
     */
    public function __construct($string)
    {
        $this->string = $string;
    }

    /**
     * @param string $needle
     *
     * @return bool
     */
    public function startsWith($needle)
    {
        return substr($this->string, 0, strlen($needle)) === $needle;
    }

    /**
     * @param string $needle
     *
     * @return bool
     */
    public function endsWith($needle)
    {
        return substr($this->string, -strlen($needle)) === $needle;
    }

    /**
     * @return bool|DateTime
     */
    public function toDateTime()
    {
        return DateTime::createFromFormat('Y-m-d H:i:s', $this->string);
    }

    /**
     * @param int $length
     *
     * @return string
     */
    public function right(int $length): string
    {
        if (strlen($this->string) < $length) {
            return $this->string;
        } else {
            return substr($this->string, strlen($this->string) - $length);
        }
    }

    /**
     * @param int $length
     *
     * @return string
     */
    public function left(int $length): string
    {
        if (strlen($this->string) < $length) {
            return $this->string;
        } else {
            return substr($this->string, 0, $length);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->string;
    }
}
