<?php

use Dracoder\Extensions\StringExtension;
use PHPUnit\Framework\TestCase;

class StringExtensionTest extends TestCase
{
    public function testGivenExistingNeedleStartFindsString()
    {
        $needle  = 'test';
        $haystack = 'testString';
        $extension = new StringExtension($haystack);
        $this->assertTrue($extension->startsWith($needle));
    }

    public function testGivenNonExistingNeedleUnableToFindString()
    {
        $needle  = 'no';
        $haystack = 'testString';
        $extension = new StringExtension($haystack);
        $this->assertFalse($extension->startsWith($needle), 'The method returned true');
    }

    public function testGivenNeedleFragmentedIsFalse()
    {
        $needle = 'test';
        $haystack = 'teseract';
        $extension = new StringExtension($haystack);
        $this->assertFalse($extension->startsWith($needle));
    }

    public function testEmptyStringInConstructorReturnsFalseOnStartWith()
    {
        $needle = 'test';
        $haystack = '';
        $extension = new StringExtension($haystack);
        $this->assertFalse($extension->startsWith($needle));
    }

    public function testGivenExistingNeedleEndsWithFindsString()
    {
        $needle  = 'test';
        $haystack = 'stringtest';
        $extension = new StringExtension($haystack);
        $this->assertTrue($extension->endsWith($needle),);
    }

    public function testGivenNonExistingNeedleUnableToFindFinishString()
    {
        $needle  = 'no';
        $haystack = 'testString';
        $extension = new StringExtension($haystack);
        $this->assertFalse($extension->endsWith($needle), 'TThe method endsWith found the string "no" on testString');
    }

    public function testGivenNeedleFragmentedIsFalseEnding()
    {
        $needle = 'test';
        $haystack = 'stringtes';
        $extension = new StringExtension($haystack);
        $this->assertFalse($extension->endsWith($needle));
    }

    public function testEmptyStringInConstructorReturnsFalseOnEndsWith()
    {
        $needle = 'test';
        $haystack = '';
        $extension = new StringExtension($haystack);
        $this->assertFalse($extension->endsWith($needle));
    }

    public function testRightGivenTheCorrectLengthReturnsTheSubstring()
    {
        $string = 'test';
        $extension = new StringExtension($string);
        $this->assertEquals($extension->right(2),'st');
    }

    public function testRightGivenALargerLengthReturnsTheString()
    {
        $string = 'test';
        $extension = new StringExtension($string);
        $this->assertEquals($extension->right(99), 'test');
    }

    public function testRightGivenANegativeLengthReturnsEmptyString()
    {
        $string = 'test';
        $extension = new StringExtension($string);
        $this->assertEquals($extension->right(-10),'');
    }

    public function testLeftGivenTheCorrectLengthReturnsTheSubstring()
    {
        $needle = 'test';
        $haystack = 'teststring';
        $extension = new StringExtension($haystack);
        $this->assertEquals($extension->left('4'),'test');
    }

    public function testLeftGivenALargeLengthReturnsTheString()
    {
        $string = 'test';
        $extension = new StringExtension($string);
        $this->assertEquals($extension->left(99), 'test');
    }

    public function testLeftGivenANegativeLengthReturnsEmptyString()
    {
        $string = 'test';
        $extension = new StringExtension($string);
        $this->assertEquals($extension->right(-10),'');
    }
}